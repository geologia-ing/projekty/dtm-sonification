If you are interested in the application of DTM Sonification in geology, I wrote an article in Polish about it
([`Wykorzystanie sonifikacji do rozpoznawania cech budowy geologicznej`](./example/Wykorzystanie_sonifikacji_do_rozpoznawania_cech_budowy_geologicznej.pdf)).
<br> 
In this folder you can find tracks which are described in the article.