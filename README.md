# DTM Sonification

## What is this project?
Sonification is a process of converting data into sound. In this project I am attempting to sonify a digital terrain model in order to hear the difference between various geological features of selected areas.

The project consists of 2 main features:
- "Crop DTM" a processing algorithm for QGIS which returns clipped DTM in the `.csv` format
- sonification package which allows to sonify DTM

## How to run the project?
### Crop DTM
In QGIS do following steps:
- Processing Tab -> Toolbox <br>
You should see the Processing Toolbox on the screen
- Processing Toolbox -> Scripts (Python logo) -> Add script to Toolbox... -> Choose `crop_dtm.py` file <br>
In the Processing Toolbox you should see category "Scripts" in the end of the algorithms list and the "Crop DTM" algorithm there
- Double click the "Crop DTM" algorithm and follow instructions in the description <br>
**Your DTM must be in the Poland 1992 Coordintate System (EPSG 2180)**
#### Dependencies
- QGIS 3.16.10 Hannover
- Crop DTM does not need any additional packages or plugins in QGIS!

### sonification package
#### Quick start - performing sonification
- import sonification package to the scope e.g. <br>
```python
from sonification import Sonification
```
- create an instance of the `Sonification` class
```python
foo = Sonification()
```
- set source DTM file (EPSG 2180 and `.csv` are only supported formats)
```python
# set_src_file(src_file, grid_interval)
foo.set_src_file("data.csv", 100)
```
- set parameters <br>
Parameters which can be modified are:
```python
'angle': None  # angle of traverse in degrees
'tempo': 60  # tempo of music in bpm
'bins': 88  # number of notes to divide the step in the traverse
'interval': None  # traverse interval (by default grid interval)
'range_min': None  # range (by default minimum in the traverse)
'range_max': None  # range (by default maximum in the traverse)
# Note: default ranges are calculated in the .run() procedure
'volume': 50  # initial volume of sound (according to MIDI standard)
'lowest_note': 21  # lowest note in the piece (21 = A0 = lowest key in the piano)
```
```python
# set_parameters(**params)
foo.set_parameters(angle=90, tempo=120)
```
- run the sonification process
```python
foo.run()
```
- save the piece to a file
```python
# save(filename)
foo.save("my_first_sonified_dtm.mid")
```
The programme will also save parameters used to generate the piece in the `.lab` file
#### Documentation and examples
For more detailed description of features and some examples visit [`example`](./example) folder 
([`manual.ipynb`](./example/manual.ipynb) notebook in particular). <br>
If you are interested in the application of DTM Sonification in geology, I wrote an article in Polish about it
([`Wykorzystanie sonifikacji do rozpoznawania cech budowy geologicznej`](./example/Wykorzystanie_sonifikacji_do_rozpoznawania_cech_budowy_geologicznej.pdf))
#### Dependencies
- Python 3.9.5
- NumPy 1.23.1 https://numpy.org/ 
- MIDIUtil 1.2.1 https://github.com/MarkCWirt/MIDIUtil

## Copyright information
- `Crop DTM.py` was written based on QGIS Script Template (under GNU License)
- sonification package with all supporting functions was written by myself


