# -*- coding: utf-8 -*-
import os

from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (QgsProcessing,
                       QgsFeatureSink,
                       QgsProcessingException,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterFeatureSink,
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingFeatureSourceDefinition,
                       QgsFeatureRequest,
                       QgsCoordinateReferenceSystem)
from qgis import processing


class CropDTM(QgsProcessingAlgorithm):
    """
    This is an algorithm which takes a raster (DTM) layer, vector polygon mask
    and returns clipped DTM in range defined by mask.
    """

    INPUT = 'INPUT'
    OUTPUT = 'OUTPUT'
    MASK = 'MASK'

    def tr(self, string):
        """
        function which translates string in order to be properly displayed
        """
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return CropDTM()

    def name(self):
        """
        returns name of function
        """
        return 'crop_DTM'

    def displayName(self):
        return self.tr('Crop DTM')

    def shortHelpString(self):
        """
        Returns a localised short helper string for the algorithm.
        """
        return self.tr("The algorithm takes a DTM layer and polygon mask. " +
                       "The result is DTM clipped by selected mask and saved as .csv file. " +
                       "In order to work as intended, the the output file must be specified. " +
                       "However the algorithm does not load the output to the project automatically. " +
                       "Note: It's not a bug, it's a feature. ")

    def initAlgorithm(self, config=None):
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.INPUT,
                self.tr('Input layer')
            )
        )

        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.MASK,
                self.tr('Mask layer'),
                [QgsProcessing.TypeVectorPolygon]
            )
        )

        self.addParameter(
            QgsProcessingParameterFeatureSink(
                self.OUTPUT,
                self.tr('Output layer')
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        # 1st step invokes GDAL clipping algorithm which returns .tif file
        clipped_tif = processing.run("gdal:cliprasterbymasklayer",
                                     {'INPUT': parameters[self.INPUT],
                                      'MASK': parameters[self.MASK],
                                      'SOURCE_CRS': QgsCoordinateReferenceSystem('EPSG:2180'),
                                      'TARGET_CRS': QgsCoordinateReferenceSystem('EPSG:2180'),
                                      'NODATA': None,
                                      'ALPHA_BAND': False,
                                      'CROP_TO_CUTLINE': True,
                                      'KEEP_RESOLUTION': False,
                                      'SET_RESOLUTION': False,
                                      'X_RESOLUTION': None,
                                      'Y_RESOLUTION': None,
                                      'MULTITHREADING': False,
                                      'OPTIONS': '',
                                      'DATA_TYPE': 0,
                                      'EXTRA': '',
                                      'OUTPUT': 'TEMPORARY_OUTPUT'},
                                     context=context,
                                     feedback=feedback,
                                     is_child_algorithm=True)
        # 2nd step converts the output .tif file into .csv
        clipped_csv = processing.run("gdal:gdal2xyz",
                                     {'INPUT': clipped_tif['OUTPUT'],
                                      'BAND': 1,
                                      'CSV': True,
                                      'OUTPUT': parameters[self.OUTPUT]},
                                     context=context,
                                     feedback=feedback,
                                     is_child_algorithm=True)

        if feedback.isCanceled():  # in order to halt algorithm externally
            return
        # 3rd step clears the .csv file and leaves only pixels which are not empty (non 0)
        # there is a risk of .csv file being extremely large
        # in order to prevent bugs, the file is not copied to memory as a whole but read line by line
        # for safety reasons the output is saved to a temporary file and then replaced
        result = parameters[self.OUTPUT].sink.asExpression().strip("'")
        temp_filename = result + '.tmp'
        with open(temp_filename, 'w') as tmp:
            with open(result, 'r') as file:
                line = file.readline()
                while line:
                    if feedback.isCanceled():
                        break
                    if line[-3:-1] != ',0':  # line is added to file if height is not 0 (value for empty pixel)
                        tmp.write(line)
                    line = file.readline()
        os.replace(temp_filename, result)
        return {self.OUTPUT: clipped_csv['OUTPUT']}
