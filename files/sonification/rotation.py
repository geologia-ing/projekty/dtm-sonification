import re
import numpy as np


def convert(s):  # converts string of coordinates to the list of floats (neglecting z-coordinate)
    tmp = s.strip().split(",")
    return [float(tmp[i]) for i in range(0, 2)]


class Rotation:  # enables to rotate a DTM by given angle and export it to a file
    # it is designed to perform multiple operations of rotations of the same DTM
    def __init__(self, src_file, grid_interval):
        self.src_file = src_file  # source DTM
        self.grid_interval = grid_interval  # DTM grid interval
        self.axis = None  # axis of revolution
        self.current_angle = 0.0  # current angle of revolution
        self.result = None  # result of rotation

    def get_angle(self):
        return self.current_angle

    def get_src(self):
        return self.src_file

    def get_axis(self):
        return self.axis

    def get_grid_interval(self):
        return self.grid_interval

    def find_axis(self):  # finds the most east point from the most north points
        max_north = 0.0
        with open(self.get_src(), 'r') as file:
            line = file.readline()
            while line:
                x = convert(line)
                if x[1] > max_north:
                    max_pts = [x]  # most north points (there might be multiple most north points)
                    max_north = x[1]
                elif x[1] == max_north:
                    max_pts.append(x)
                line = file.readline()
        max_east = 0.0
        ax = None
        for x in max_pts:
            if x[0] > max_east:
                max_east = x[0]
                ax = x
        return ax

    def rotate(self, angle):  # rotates DTM by the given angle around the most north-east point
        if self.axis is None:
            self.axis = np.array(self.find_axis())
        ax = self.get_axis()
        angle_rad = np.radians(angle)
        r = np.array(
            [[np.cos(angle_rad), -np.sin(angle_rad)], [np.sin(angle_rad), np.cos(angle_rad)]])  # rotation matrix
        self.result = []
        with open(self.get_src(), 'r') as tmp:
            line = tmp.readline()
            while line:
                x = np.array(convert(line)) - ax  # shifts the origin of coordinate system to the center of revolution
                x = r @ x + ax  # rotates the point and transforms back to initial coordinate system
                # z-coordinate is concatenated to the new point
                z = [float(line.split(",")[2])]
                self.result.append(np.append(x, z))
                line = tmp.readline()
        self.current_angle = angle
        self.result = np.array(self.result, dtype=object)

    def export(self, output=None):  # saves to a file
        if output is None:  # by default the file is named 'src_file_rotated_angle.csv'
            name, ext = re.split("..", self.get_src())
            output = name + '_rotated_' + str(self.get_angle()) + '.' + ext
        with open(output, 'w') as out:
            if self.result is not None:
                for x in self.result:
                    out.write(str(x[0]) + "," + str(x[1]) + "," + str(x[2]) + '\n')

    def __str__(self):
        return str(self.result)
