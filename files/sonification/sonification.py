from .traverse import Traverse
import numpy as np
from midiutil import MIDIFile


class Sonification:
    def __init__(self):
        self.traverse = None  # Traverse
        self.sound = None  # MIDI sound
        self.parameters = {'angle': None,  # angle of traverse in degrees
                           'tempo': 60,  # tempo of music in bpm
                           'bins': 88,  # number of notes to divide the step in the traverse
                           'interval': None,  # traverse interval (by default grid interval)
                           'range_min': None,  # range (by default minimum in the traverse)
                           'range_max': None,  # range (by default maximum in the traverse)
                           # Note: default ranges are calculated in the .run() procedure
                           'volume': 50,  # initial volume of sound
                           'lowest_note': 21  # lowest note in the piece (21 = A0 = lowest key in the piano)
                           }
        # boolean values for safety control and optimisation
        self.parameters_set = False  # states whether parameters are correctly set
        self.angle_changed = True  # states whether the traverse needs computation
        self.interval_changed = True  # states whether the traverse needs computation
        self.file_changed = True  # states whether the traverse needs computation

    def set_src_file(self, src_file, grid_interval):  # sets an input file for the sonification process
        with open(src_file, 'r'):  # for safety reasons (it will raise a FileNotFoundException if src_file is not valid)
            pass
        if not isinstance(grid_interval, int) and not isinstance(grid_interval, float):  # another safety check
            raise RuntimeError("Invalid interval")
        self.traverse = Traverse(src_file, grid_interval)  # traverse initialisation
        self.parameters['interval'] = self.traverse.get_grid_interval()  # default interval initialisation
        self.interval_changed = True
        self.file_changed = True
        self.parameters['range_min'] = None
        self.parameters['range_max'] = None

    def check_dtm(self):  # checks whether DTM file is set
        if self.traverse is None:
            raise RuntimeError("Source DTM is not set")

    def get_src_file(self):
        self.check_dtm()
        return self.traverse.get_src()

    def set_parameters(self, **params):  # sets parameters for the sonification process
        self.check_dtm()
        self.parameters_set = False  # if the function does not end properly the program should not be able to proceed
        # variables which allow to save time if crucial parameters haven't changed
        old_interval = self.parameters['interval']
        old_angle = self.parameters['angle']
        self.parameters = self.parameters | params  # update of parameters
        if self.parameters['angle'] != old_angle:
            self.angle_changed = True
        # safety check if angle is a number
        if not isinstance(self.parameters['angle'], int) and not isinstance(self.parameters['angle'], float):
            raise RuntimeError("Invalid angle")
        if self.parameters['interval'] != old_interval:
            self.interval_changed = True
        # safety check if interval is a number
        if not isinstance(self.parameters['interval'], int) and not isinstance(self.parameters['interval'], float):
            raise RuntimeError("Invalid interval")
        self.parameters_set = True

    def check_parameters(self):  # checks whether parameters are set
        if not self.parameters_set:
            raise RuntimeError("Parameters are not set")

    def get_parameters(self):
        self.check_parameters()
        return self.parameters

    def traverse_changed(self):  # checks whether traverse needs to be computed
        return self.interval_changed or self.angle_changed or self.file_changed

    def compute_traverse(self):
        # safety checks
        self.check_dtm()
        self.check_parameters()
        if self.file_changed or self.angle_changed:
            # if angle or file are changed, the DTM must be rotated and traversed again
            self.traverse.rotate(self.parameters['angle'])
            self.file_changed = False
            self.angle_changed = False
            self.traverse.make_traverse(self.parameters['interval'])
            self.interval_changed = False
        if self.interval_changed:
            # if only interval is changed, the rotation is unnecessary
            self.traverse.make_traverse(self.parameters['interval'])
            self.interval_changed = False

    def traverse_count(self):  # allows to see if the traverse is well balanced
        # returns the list of numbers of DTM pixels in each traverse step
        if self.traverse_changed():
            self.compute_traverse()
        return self.traverse.peek_count()

    def run(self):  # performs sonification
        if self.traverse_changed():
            self.compute_traverse()
        # range calculations
        if self.parameters['range_min'] is None:
            self.parameters['range_min'] = np.min(self.traverse.result[:, 2])
        if self.parameters['range_max'] is None:
            self.parameters['range_max'] = np.max(self.traverse.result[:, 2])
        # creation of MIDI sound
        self.sound = MIDIFile(1)
        track = 0
        time = 0  # start time
        channel = 0
        duration = 1  # in beats
        self.sound.addTempo(track, time, self.parameters['tempo'])
        for count, line in enumerate(self.traverse.get_traverse()):
            hist = np.histogram(np.array(line),
                                bins=self.parameters['bins'],
                                range=(self.parameters['range_min'], self.parameters['range_max']))[0]
            # hist is an np.array of numbers of pixels corresponding to a certain note respectively
            mask = np.where(hist != 0)  # omit notes which don't have any pixels
            for i in mask[0]:
                self.sound.addNote(track,
                                   channel,
                                   # shift from the lowest note (in semitones on the piano keyboard)
                                   self.parameters['lowest_note'] + 88//self.parameters['bins'] * i,
                                   time + count,
                                   duration,
                                   # volume control (the more pixels, the louder sound)
                                   min(self.parameters['volume'] + 5 * hist[i], 127))

    def check_sound(self):  # checks whether the .run() procedure has been executed
        if self.sound is None:
            raise RuntimeError("Sound is not created")

    def get_resolution(self):  # returns the resolution of traverse i.e. the range of every bin
        # safety checks
        self.check_parameters()
        self.check_sound()
        return (self.parameters['range_max']-self.parameters['range_min'])/self.parameters['bins']

    def save(self, filename):  # exports the piece created to a file in the MIDI standard
        # as well as parameters to a text file (with .lab extension)
        self.check_sound()  # safety check
        with open(filename, "wb") as output_file:
            self.sound.writeFile(output_file)
        with open(filename + ".lab", "w") as label_file:
            params = self.parameters
            for x in params:
                label_file.write(x + ': ' + str(params[x]) + '\n')
            label_file.write('src_file' + ': ' + str(self.get_src_file()) + '\n')
            label_file.write('resolution' + ': ' + str(self.get_resolution()) + '\n')
