from .rotation import Rotation
import numpy as np


class Traverse(Rotation):  # enables to traverse the rotated DTM, derived from class Rotation
    def __init__(self, src_file, grid_interval):
        super().__init__(src_file, grid_interval)
        self.traverse = None  # traversed DTM
        self.current_interval = None  # interval of division used to traverse the DTM

    def make_traverse(self, interval=None):  # makes traverse with given interval
        if self.result is None:
            raise RuntimeError("DTM is not rotated")
        # the main purpose of traverse is to divide rotated DTM into chunks of given width
        long = self.result[:, 0]  # longitudes (x-coordinate in the CRS)
        west = np.min(long)  # most west coordinate
        east = np.max(long)  # most east coordinate
        if interval is None:  # if interval is not given the procedure takes default grid interval from the DTM
            interval = self.get_grid_interval()
        # the goal is to put z-coordinates in appropriate lists corresponding to certain y-coordinate bands
        correction = np.floor(west)
        output = [[] for i in range(0, int((np.floor(east) - correction) / interval) + 1)]  # initialisation of the list
        # we make use of the following formula, which enables to complete the task in linear time (avoiding sorting)
        for x in self.result:
            output[int((np.floor(x[0]) - correction) / interval)].append(x[2])
        self.traverse = output
        self.current_interval = interval

    def export(self, output=None):  # saves to a file
        if output is None:  # by default the file is named 'traverse_src_file.csv'
            output = 'traverse_' + self.get_src()
        with open(output, 'w') as out:
            if self.traverse is not None:
                for x in self.traverse:
                    out.write(str(x) + '\n')

    def peek_count(self):  # helper function to see whether interval and angle parameters are correctly adjusted
        if self.traverse is not None:
            return [len(x) for x in self.traverse]

    def get_interval(self):
        return self.current_interval

    def get_traverse(self):
        if self.traverse is not None:
            return self.traverse

    def __str__(self):
        return str(self.traverse)
